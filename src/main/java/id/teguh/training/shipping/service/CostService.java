package id.teguh.training.shipping.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Service
public class CostService extends ShippingService {
    private Logger LOGGER = LoggerFactory.getLogger(CostService.class);

    public Object checkDeliveryPrice(String origin, String destination, String weight, String courier) {
        LOGGER.info("check delivery cost origin {}, destination {}, weight {}, courier {}", origin, destination, weight, courier);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("origin", origin);
        params.add("destination", destination);
        params.add("weight", weight);
        params.add("courier", courier);

        HttpHeaders httpHeaders = getHttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        ResponseEntity<String> responseEntity = restTemplate.exchange(rajaOngkirProperties.getUrl() + "/cost", HttpMethod.POST, new HttpEntity(params, httpHeaders), String.class);
        Object data = parse(responseEntity);
        return new ResponseEntity<>(data, HttpStatus.OK);

    }
}
