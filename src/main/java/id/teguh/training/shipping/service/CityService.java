package id.teguh.training.shipping.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class CityService extends ShippingService {
    private Logger LOGGER = LoggerFactory.getLogger(CityService.class);

    public Object getCities(String provinceId, String cityId) {
        LOGGER.info("get data city provinceId {} and cityId {}", provinceId, cityId);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(rajaOngkirProperties.getUrl() + "/city")
                .queryParam("id", cityId)
                .queryParam("province", provinceId);

        ResponseEntity<String> responseEntity = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, new HttpEntity(getHttpHeaders()), String.class);
        return parse(responseEntity);
    }
}
