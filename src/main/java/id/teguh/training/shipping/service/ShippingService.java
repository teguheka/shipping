package id.teguh.training.shipping.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.teguh.training.shipping.RajaOngkirProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class ShippingService {

    @Autowired protected RestTemplate restTemplate;
    @Autowired protected RajaOngkirProperties rajaOngkirProperties;


    protected HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("key", rajaOngkirProperties.getKey());

        return httpHeaders;
    }

    protected Object parse(ResponseEntity<String> responseEntity) {
        if (HttpStatus.OK == responseEntity.getStatusCode()) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                Map responseBody = mapper.readValue(responseEntity.getBody(), HashMap.class);
                return responseBody.get("rajaongkir");
            } catch (Exception e) {
                throw new RuntimeException("JsonException, message: " + e.getMessage());
            }
        } else {
            throw new RuntimeException("HttpStatus: " + responseEntity.getStatusCode() + ", error: " + responseEntity.getBody());
        }
    }
}
