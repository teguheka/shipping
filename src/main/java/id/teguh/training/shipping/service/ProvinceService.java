package id.teguh.training.shipping.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ProvinceService extends ShippingService {
    private Logger LOGGER = LoggerFactory.getLogger(ProvinceService.class);

    public Object getProvinces() {
        LOGGER.info("get data province");
        ResponseEntity<String> responseEntity = restTemplate.exchange(rajaOngkirProperties.getUrl() + "/province", HttpMethod.GET, new HttpEntity(getHttpHeaders()), String.class);
        return parse(responseEntity);
    }
}
