package id.teguh.training.shipping.controller;

import id.teguh.training.shipping.service.CostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/cost")
@Api(description = "Set of endpoints for get data cost.")
public class CostController {
    @Autowired private CostService costService;

    @GetMapping("")
    @ApiOperation("Check delivery price")
    public ResponseEntity<?> getListProvince(@ApiParam(value="origin city id", required = true) @RequestParam String origin,
                                             @ApiParam(value="destination city id", required = true) @RequestParam String destination,
                                             @ApiParam(value="in gram", required = true) @RequestParam String weight,
                                             @ApiParam(allowableValues = "jne,tiki,pos", required = true) @RequestParam String courier) {
        Object data = costService.checkDeliveryPrice(origin, destination, weight, courier);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
