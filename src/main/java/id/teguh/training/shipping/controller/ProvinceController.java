package id.teguh.training.shipping.controller;


import id.teguh.training.shipping.service.ProvinceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/provinces")
@Api(description = "Set of endpoints for get data of province.")
public class ProvinceController {
    @Autowired private ProvinceService provinceService;

    @GetMapping("")
    @ApiOperation("Get list of province")
    public ResponseEntity<?> getListProvince() {
        Object data = provinceService.getProvinces();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
