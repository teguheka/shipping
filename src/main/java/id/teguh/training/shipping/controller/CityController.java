package id.teguh.training.shipping.controller;

import id.teguh.training.shipping.service.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/cities")
@Api(description = "Set of endpoints for get data of city.")
public class CityController {
    @Autowired private CityService cityService;

    @GetMapping("")
    @ApiOperation("Get list of city")
    public ResponseEntity<?> getListCity(@RequestParam(required = false) String provinceId,
                                         @RequestParam(required = false) String cityId) {
        Object data = cityService.getCities(provinceId, cityId);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
